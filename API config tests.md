# Ansible EOS API Config Testing

## Setup

### Create venv and containers

```bash
virtualenv .venv && source .venv/bin/activate

pip install ansible ansible-pylibssh

docker import cEOS64-lab-4.28.1F.tar.xz ceos:4.28.1F_x64

# The management interface will bind to docker0
sudo ip -6 a a ff::1/64 dev docker0

# This will be eth1 on the EOS device
docker network create --driver bridge docker1

docker create \
--name ceos_ansible_1 \
--privileged \
-t \
-e INTFTYPE=eth \
-e ETBA=1 \
-e SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 \
-e CEOS=1 \
-e EOS_PLATFORM=ceoslab \
-e container=docker \
-e MAPETH0=1 \
-e MGMT_INTF=eth0 \
ceos:4.28.1F_x64 \
/sbin/init systemd.setenv=INTFTYPE=eth systemd.setenv=ETBA=1 systemd.setenv=SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 systemd.setenv=CEOS=1 systemd.setenv=EOS_PLATFORM=ceoslab systemd.setenv=container=docker systemd.setenv=MAPETH0=1 systemd.setenv=MGMT_INTF=eth0

docker network connect docker1 ceos_ansible_1
```

### Start containers

```bash
docker start ceos_ansible_1
```

### Connect to EOS CLI
```bash
docker exec -it ceos_ansible_1 Cli
```

### Stop containers

```bash
docker stop ceos_ansible_1
```

### Stop and remove containers

```bash
docker stop ceos_ansible_1 && docker rm ceos_ansible_1
```

### Base device configs

This is to allow remote access from Ansible, using a self signed cert:

```
enable

security pki key generate rsa 2048 eapiServer.key
security pki certificate generate self-signed eapiServer.cert key eapiServer.key validity 365 parameters common-name self-signed

configure

hostname ceos-ansible-1

username admin privilege 15 secret 0 admin

ip routing

ipv6 unicast-routing

interface Management0
   ip address 172.17.0.251/16
   ipv6 enable
   ipv6 address ff::251/64
   exit

management security
   ssl profile eapi
   certificate eapiServer.cert key eapiServer.key
   exit

management http-server
   protocol https ssl profile eapi
   exit

management api http-commands
   no shutdown
   exit

exit

wr
```
&nbsp;


## API Testing

```
ansible-playbook -vvvv -i hosts eapi_config.yml -C --tags v4
ansible-playbook -vvvv -i hosts eapi_config.yml -C --tags v6
```

Testing via the API produces the same results as via the CLI, in this case, the same CLI commands are being packages as JSON and send over the REST API.

