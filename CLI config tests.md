# Ansible EOS CLI Config Testing

## Setup

### Create venv and containers

```bash
virtualenv .venv && source .venv/bin/activate

pip install ansible ansible-pylibssh

docker import cEOS64-lab-4.28.1F.tar.xz ceos:4.28.1F_x64

# The management interface will bind to docker0
sudo ip -6 a a ff::1/64 dev docker0

# This will be eth1 on the EOS device
docker network create --driver bridge docker1

docker create \
--name ceos_ansible_1 \
--privileged \
-t \
-e INTFTYPE=eth \
-e ETBA=1 \
-e SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 \
-e CEOS=1 \
-e EOS_PLATFORM=ceoslab \
-e container=docker \
-e MAPETH0=1 \
-e MGMT_INTF=eth0 \
ceos:4.28.1F_x64 \
/sbin/init systemd.setenv=INTFTYPE=eth systemd.setenv=ETBA=1 systemd.setenv=SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 systemd.setenv=CEOS=1 systemd.setenv=EOS_PLATFORM=ceoslab systemd.setenv=container=docker systemd.setenv=MAPETH0=1 systemd.setenv=MGMT_INTF=eth0

docker network connect docker1 ceos_ansible_1
```

### Start containers

```bash
docker start ceos_ansible_1
```

### Connect to EOS CLI
```bash
docker exec -it ceos_ansible_1 Cli
```

### Stop containers

```bash
docker stop ceos_ansible_1
```

### Stop and remove containers

```bash
docker stop ceos_ansible_1 && docker rm ceos_ansible_1
```

### Base device configs

This is to allow remote access from Ansible:

```
enable

configure

hostname ceos-ansible-1

username admin privilege 15 secret 0 admin

ip routing

ipv6 unicast-routing

interface Management0
   ip address 172.17.0.251/16
   ipv6 enable
   ipv6 address ff::251/64
   exit

exit

wr
```
&nbsp;

## CLI Testing

```
ansible-playbook -vvvv -i hosts cli_config.yml -C --tags v4
ansible-playbook -vvvv -i hosts cli_config.yml -C --tags v6
```

The following table documents the results from testing the various ansible check and diff CLI options with the Ansible arista.eos.eos_config module, to see how we could get config diffs and review them before applying them.

**diff_against**:  
When using the ansible-playbook --diff command line argument the module can generate diffs against different sources.

When this option is configure as startup, the module will return the diff of the running-config against the startup-config.

When this option is configured as intended, the module will return the diff of the running-config against the configuration provided in the intended_config argument.

When this option is configured as running, the module will return the before and after diff of the running-config with respect to any changes made to the device configuration.

When this option is configured as session, the diff returned will be based on the configuration session.

Choices:
```
    startup
    running
    intended
    session ← (default)
```

**replace**:  
Instructs the module on the way to perform the configuration on the device. If the replace argument is set to line then the modified lines are pushed to the device in configuration mode. If the replace argument is set to block then the entire command block is pushed to the device in configuration mode if any line is not correct. (Although not documented, "config" is a full config replace).

Choices:
```
    line ← (default)
    block
    config
```

### Results

Test applying config commands which add additional lines of config:

| Ansible Command | arista.eos.eos_config | Result | Suggeston |
|:---|:---|:---|:---|
|ansible-playbook -C | replace: line</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows only the lines which would be added (`src` without the conifg lines which already exist on the device). The `diff` value shows what the config would be if the missing lines in `updates` were added. | :heavy_check_mark: Allows for diff without change. |
|ansible-playbook -D | replace: line</br> diff_against: session | Makes the config change without prompt. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows only the lines which will be added (`src` without the conifg lines which already exist on the device). The `diff` value shows the new config including the missing lines in `updates` which were added. The config is merged with existing lines. | :heavy_check_mark: Allows for diff and change. |
|ansible-playbook -CD | replace: line</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows only the lines which would be added (`src` without the conifg lines which already exist on the device). The `diff` value shows what the config would be if the lines from `updates` were added. | :x: Not needed. |
|ansible-playbook -C | replace: line</br> diff_against: running | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows only the lines which would be added (`src` without the conifg lines which already exist on the device). There is no diff. | :x: Doesn't produce a diff. |
|ansible-playbook -D | replace: line</br> diff_against: running | Makes the config change without prompt. The `before` and `after` values contain the full device config before and after the config lines where added. The `src` value shows all the config to be added. The `updates` value shows only the lines which were added (`src` without the conifg lines which already exist on the device). The config is merged with existing lines. | :x: Doesn't produce a diff. |
|ansible-playbook -CD | replace: line</br> diff_against: running | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows only the lines which would be added (`src` without the conifg lines which already exist on the device). There is no diff. | :x: Doesn't produce a diff. |
|ansible-playbook -C | replace: block</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `lines` value contains the full config to be applied. The `updates` value is the same as `lines`. The `diff` value shows a diff just for the block being changed. | :x: Not needed. |
|ansible-playbook -D | replace: block</br> diff_against: session | Makes the config change without prompt. The `before` and `after` values are null. The `lines` value contains the full config to be applied. The `updates` value is the same as `lines`. The `diff` value shows a diff just for the block being changed. The config is merged with existing lines. | :x: Not needed. |
|ansible-playbook -CD | replace: block</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `lines` value contains the full config to be applied. The `updates` value is the same as `lines`. The `diff` value shows a diff just for the block being changed. | :x: Not needed. |
&nbsp;

Test applying config comands which remove lines of config:

| Ansible Command | arista.eos.eos_config | Result | Suggestion |
|:---|:---|:---|:---|
|ansible-playbook -C | replace: line</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows the same as `src`, the lines of config to be added/exeuted. The `diff` value shows what the config would be if the lines in `src` were executed. | :heavy_check_mark: Allows for diff without change. |
|ansible-playbook -D | replace: line</br> diff_against: session | Makes the config changes without prompt. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows the same as `src`, the lines of config to be added/exeuted. The `diff` value shows the new config including the lines in `src` which were removed. The config is merged with existing lines. | :heavy_check_mark: Allows for diff and change. |
|ansible-playbook -CD | replace: line</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows the same as `src`, the lines of config to be added/exeuted. The `diff` value shows what the config would be if the lines in `src` were executed. | :x: Not needed. |
|ansible-playbook -C | replace: line</br> diff_against: running | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows the same as `src`, the lines of config to be added/exeuted. There is no diff. | :x: Doesn't produce a diff. |
|ansible-playbook -D | replace: line</br> diff_against: running | Makes the config changes without prompt. The `before` and `after` values are the full device config before and after the config changes are applied. The `src` value shows all the config to be added. The `updates` value shows the same as `src`, the lines of config to be added/exeuted. There is no diff. The config is merged with existing lines. | :x: Doesn't produce a diff. |
|ansible-playbook -CD | replace: line</br> diff_against: running | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows all the config to be added. The `updates` value shows the same as `src`, the lines of config to be added/exeuted. There is no diff. | :x: Doesn't produce a diff. |
|ansible-playbook -C | replace: block</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `lines` value contains the full config to be applied. The `updates` value is the same as `lines`. The `diff` value shows a diff just for the block being changed. | :x: Not needed. |
|ansible-playbook -D | replace: block</br> diff_against: session | Makes the config change without prompt. The `before` and `after` values are null. The `lines` value contains the full config to be applied. The `updates` value is the same as `lines`. The `diff` value shows a diff just for the block being changed. The config is merged with existing lines. | :x: Not needed. |
|ansible-playbook -CD | replace: block</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `lines` value contains the full config to be applied. The `updates` value is the same as `lines`. The `diff` value shows a diff just for the block being changed. | :x: Not needed. |

Testing full config replace:

| Ansible Command | arista.eos.eos_config | Result | Suggestion |
|:---|:---|:---|:---|
|ansible-playbook -C | replace: config</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows the new full device config. The `updates` value shows the same as `src`, the new full config. The `diff` value shows the difference between the current config and new config. | :heavy_check_mark: Allows for diff without change. |
|ansible-playbook -D | replace: config</br> diff_against: session | Makes the config changes without prompt. The `before` and `after` values are null. The `src` value shows the new full device config. The `updates` value shows the same as `src`, the new full config. The `diff` value shows the difference between the old config and the new config. | :heavy_check_mark: Allows for diff and change. |
|ansible-playbook -CD | replace: config</br> diff_against: session | Doesn't make the config change. The `before` and `after` values are null. The `src` value shows the new full device config. The `updates` value shows the same as `src`, the new full config. The `diff` value shows the difference between the current config and new config. | :x: Not needed. |


### Results explained

When `replace` is `line` or `block`, the candidate config is merged/added. For example:

Candiate config (partial block which adds a new line):
```
interface Ethernet 1
   description test link to ceos_ansible_2
```

Running config:
```
interface Ethernet1
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```

`replace: line` resulting config:
```
interface Ethernet1
   description test link to ceos_ansible_2
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```

`replace: block` resulting config:
```
interface Ethernet1
   description test link to ceos_ansible_2
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```
&nbsp;


Candiate config (partial block which removes a new line):
```
interface Ethernet 1
   no description
```

Running config:
```
interface Ethernet1
   description test link to ceos_ansible_2
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```

`replace: line` resulting config:
```
interface Ethernet1
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```

`replace: block` resulting config:
```
interface Ethernet1
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled

```
&nbsp;


Candiate config (full block with a line removed):
```
interface Ethernet 1
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```

Running config:
```
interface Ethernet1
   description test link to ceos_ansible_2
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```

`replace: line` resulting config:
```
interface Ethernet1
   description test link to ceos_ansible_2
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```

`replace: block` resulting config:
```
interface Ethernet1
   description test link to ceos_ansible_2
   no switchport
   ip address 10.0.0.1/31
   ipv6 enable
   ipv6 address 2001:db8:1:2::1/64
   ipv6 nd ra disabled
```
