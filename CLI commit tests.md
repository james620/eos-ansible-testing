# Ansible EOS CLI Commit Testing

## Setup

### Create venv and containers

```bash
virtualenv .venv && source .venv/bin/activate

pip install ansible ansible-pylibssh

docker import cEOS64-lab-4.28.1F.tar.xz ceos:4.28.1F_x64

# The management interface will bind to docker0
sudo ip -6 a a ff::1/64 dev docker0

# This will be eth1 on the EOS device
docker network create --driver bridge docker1

docker create \
--name ceos_ansible_1 \
--privileged \
-t \
-e INTFTYPE=eth \
-e ETBA=1 \
-e SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 \
-e CEOS=1 \
-e EOS_PLATFORM=ceoslab \
-e container=docker \
-e MAPETH0=1 \
-e MGMT_INTF=eth0 \
ceos:4.28.1F_x64 \
/sbin/init systemd.setenv=INTFTYPE=eth systemd.setenv=ETBA=1 systemd.setenv=SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 systemd.setenv=CEOS=1 systemd.setenv=EOS_PLATFORM=ceoslab systemd.setenv=container=docker systemd.setenv=MAPETH0=1 systemd.setenv=MGMT_INTF=eth0

docker network connect docker1 ceos_ansible_1
```

### Start containers

```bash
docker start ceos_ansible_1
```

### Connect to EOS CLI
```bash
docker exec -it ceos_ansible_1 Cli
```

### Stop containers

```bash
docker stop ceos_ansible_1
```

### Stop and remove containers

```bash
docker stop ceos_ansible_1 && docker rm ceos_ansible_1
```

### Base device configs

This is to allow remote access from Ansible:

```
enable

configure

hostname ceos-ansible-1

username admin privilege 15 secret 0 admin

ip routing

ipv6 unicast-routing

interface Management0
   ip address 172.17.0.251/16
   ipv6 enable
   ipv6 address ff::251/64 # SSH won't work with link local IP :(
   exit

# These are required for SCP to work
aaa authentication login console local
aaa authorization exec default local

exit

wr
```
&nbsp;

## CLI Testing

```
ansible-playbook -vvvv -i hosts cli_commit.yml -C --tags v4
ansible-playbook -vvvv -i hosts cli_commit.yml -C --tags v6
```

### Test Results

Session config is implicitly applied when loaded, even if `commit` is not issued.The following task to manually create a diff for review, will actualy commit the config because it wasn't explicitly aborted after being loaded:

```
- name: Apply replacement config
  arista.eos.eos_command:
    commands:
      - configure session {{ session }}
      - copy file:/tmp/full_config.txt session-config
      - show session-config diffs
```

When creating a session with `configure session {{ session }}`, it can be aborted with `configure session {{ session }} abort`. In the above example, this would be required manually to create a diff (as opposed to using the arista.eos.eos_config module).

If config is commited inside a session, the session is automatically ended.

If a config lock is enabled, aborting a session or commiting the session changes, do not unlock the config.


### Recommendations

There are different ways we could generate config, push it to the device, create a diff, and commit it etc...

One method is to generate the config locally, SCP it to the device, then generate a diff on the device, like the following:

```
- name: Generate config
  ansible.builtin.template:
    src: full_config.j2
    dest: /tmp/full_config.txt
  delegate_to: localhost

#- name: Copy new config to device
#  ansible.netcommon.net_put:
#    src: /tmp/full_config.txt
#    dest: /tmp/full_config.txt
#    protocol: scp
# scp /tmp/full_config.txt admin@172.17.0.251:/tmp/full_config.txt

- name: Create a config diff
  arista.eos.eos_config:
    src: /tmp/full_config.txt
    diff_against: session
    replace: config
  check_mode: yes
  register: diff_output

- name: Print the diff
  debug:
    var: diff_output

- name: Apply replacement config
  arista.eos.eos_command:
    commands:
      - configure checkpoint save {{ session }}
      - configure session {{ session }}
      - copy file:/tmp/full_config.txt session-config
      - configure session {{ session }} commit timer 00:05:00

- name: Confirm the commit
  arista.eos.eos_command:
    commands:
      - configure session {{ session }} commit

- name: Save changes to startup config
  arista.eos.eos_command:
    commands: copy running-config startup-config
```

The problems with the above approach are:

* ansible.netcommon.net_put doesn't seem to be working with Arista (and of course, it doesn't work with ansible.netcommon.httpapi only ansible.netcommon.network_cli).
* The eos_config module doesn't support config locking, so this has to be done manually before and after using the eos_command module.

The benefits of this approach are:

* It supports a commit timeout.
* It allows for the user (or an Ansible task) to reject the commit and have the change rollback immediataly instead of waiting for the timeout.

&nbsp;

Another method is to get the device to generate the diff:

```
- name: Generate config
  ansible.builtin.template:
    src: full_config.j2
    dest: /tmp/full_config.txt
  delegate_to: localhost

- name: Create a config diff
  arista.eos.eos_config:
    src: /tmp/full_config.txt
    diff_against: session
    replace: config
  check_mode: yes
  register: diff_output

- name: Print the diff
  debug:
    var: diff_output

- name: Apply new config
  arista.eos.eos_config:
    src: /tmp/full_config.txt
    diff_against: session
    replace: config
    save_when: changed
```

The problems with this approach are:

* The eos_config module doesn't support config locking, so this has to be done manually before and after using the eos_command module.
* The eos_config module uses a src on the Ansible host, so this method transfers the full config to the device twice.
* The eos_config module doesn't support a commit with rollback timer.

The benefits with this approach are:

* The config is auto saved to startup-config if it was changed.
